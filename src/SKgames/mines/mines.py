# %%
import random
import os
from enum import IntFlag, auto, IntEnum
from itertools import repeat
import argparse
import math
from functools import cached_property, cache
import time

import msvcrt

class Keys(IntEnum):
    SPACE = 0x20
    ENTER = 0x0d
    ESCAPE = 0x1b
    EIGHT = 0x38
    SIX = 0x36
    TWO = 0x32
    FOUR = 0x34

class SpecialKeys(IntEnum):
    SPECIAL1 = 0x00
    SPECIAL2 = 0xe0
    UP = 0x48
    RIGHT = 0x4d
    DOWN = 0x50
    LEFT = 0x4b
#%%

class Cells:
    """A Cells object is meant to represent one index in a 1D memoryarray,
    maybe of any mutable sequence of integers. Only tested with bytearrays,
    that is only single bytes--not 4-byte integers.
    
    A Cells object manages the item at its own index (and only its own index).

    It has access to a list of other Cells objects that are instances of the
    same Cells class.

    #### Usage ####
    You have to subclass Cells to use it, see __init_subclass__.
    
    >>> class _Cells(Cells, field, field_shape, str_format)
    
    From now on, `_Cells` refers specifically to the subclass, while `Cells`
    may refer to the base class.

    - `field` should be a memoryview of the bytearray.
    - `field_shape` should be a tuple `(h, w)` representing the intended shape of the field.
    - `str_format` is option, and should be a callable taking as its only argument a byte or
    integer and returning a string representation. Defaults to `lambda x:f"int(x):0>2x"`

    Create an instance with `cell = _Cells(i)` where `i` is the index the new
    `cell` object should represent in the memoryview `_Cells._field`.

    Cells supports bitwise operators, and interacting with the `_Cells` object is
    like interacting directly with the original memoryview object:

    >>> cell = _Cells(0)
    >>> cell._value
    ... 1
    >>> _Cells._field[0]
    ... 1
    >>> cell.set_bit(4)
    >>> _Cells._field[0]
    ... 5

    #### Class Attributes ####
    These subclassing arguments are accessible from within a subclass:
    - `_Cells._field`           A memoryview object
    - `_Cells._field_shape`     A tuple, the shape of the 1D _field memoryarray, 

    There are also other attributes.
    - `Cells.map`            a dict mapping indexes to _Cells objects.
    - `Cells.Neighbors`      an IntFlags enum:
        - `RIGHT = auto()`
        - `DOWN = auto()`
        - `LEFT = auto()`
        - `UP = auto()`
        - `DOWN_RIGHT = RIGHT | DOWN`
        - `DOWN_LEFT = LEFT | DOWN`
        - `UP_RIGHT = RIGHT | UP`
        - `UP_LEFT = LEFT | UP`
    
    Each member of `Cells.Neighbors` has some methods.
    
    `Cells.Neighbors.relative_index(index, width)`
    `index` is an index into an array, and width is the width component of a shape.
    No edge detection takes place.
    >>> _Cells.Neighbors.DOWN.relative_index(4, width=10)
    ... 14
    >>> _Cells.Neighbors.DOWN_RIGHT.relative_index(4, width=10)
    ... 15
    >>> # No edge detection:
    >>> _Cells.Neighbors.RIGHT.relative_index(9, 10)
    ... 10

    `Cells.Neighbors.wrap(index, shape)`
    `index` is an index into an array, and `shape` is a tuple of integers, indicating height and width.
    `Cells.Neighbors.wrap` calculates the index "opposite" the provided `index`, assuming a grid with the
    shape `shape`
    >>> _Cells.Neighbors.LEFT.wrap(0, (10, 10))
    ... 9
    >>> _Cells.Neighbors.DOWN.wrap(94, (10, 10))
    ... 4

    ### Instance properties ###
    - `cell.edges`        The computed edges near this cell, as a `Cells.Neighbors` IntFlag.
    - `cell.neighbors`    This cell's neighbors, honoring edges, as a list of `_Cells` objects.
    - `cell._index`       The index into `_Cells._field` for which this `_Cells` object is responsible.
    - `cell._value`       The current value at `_Cells._field[cell._index]`

    ### Instance methods ###
    - `cell.bit_is_set(bit)`  Equivalent to `_Cells._field[cell._index] & bit`
    - `cell.set_bit(bit)`     Equivalent to `_Cells._field[cell._index] |= bit`
    - `cell.unset_bit(bit)`   Equivalent to `_Cells._field[cell._index] &= ~bit`
    - `cell.toggle_bit(bit)`  Equivalent to `_Cells._field[cell._index] ^= bit`
    - `cell.as_string()       Calls the function passed in when subclassing on this `cell`'s `_value`; defaults to `str()`
    """
    all_cells = dict()
    _index = None
    _field = None
    _field_shape = None
    as_string = None

    @cached_property
    def edges(self):
        edges = 0
        if self._index % self._field_shape[0] == 0:
            edges |= Cells.Neighbors.LEFT
        if self._index % self._field_shape[0] == self._field_shape[0] - 1:
            edges |= Cells.Neighbors.RIGHT
        if self._index // self._field_shape[0] == 0:
            edges |= Cells.Neighbors.UP
        if self._index // self._field_shape[0] == self._field_shape[1] - 1:
            edges |= Cells.Neighbors.DOWN
        return edges
    
    @cached_property
    def neighbors(self) -> list[int]:
        """Returns all neighbor indexes. Use Field.Neighbors"""
        neighbors = []
        for direction in Cells.Neighbors.__members__.values():
            if direction & self.edges > 0:
                continue
            neighbors.append(Cells.Neighbors(direction).relative_index(self._index, self._field_shape[0]))
        return neighbors

    @property
    def _value(self):
        return self._field[self._index]


        # Used in locating neighbors' indices
    class Neighbors(IntFlag):
        RIGHT = auto()
        DOWN = auto()
        LEFT = auto()
        UP = auto()
        DOWN_RIGHT = RIGHT | DOWN
        DOWN_LEFT = LEFT | DOWN
        UP_RIGHT = RIGHT | UP
        UP_LEFT = LEFT | UP
        
        @cache
        def relative_index(self, index: int, width: int) -> int:
            """Results relative to the index passed in, in the direction you
            called it on.
            i.e. Field.Neighbors.RIGHT.relative_index(i, w) returns the index
            of the neighbor to the right."""
            # Just math moving in a direction on a 2D grid from i
            MATHS = {Cells.Neighbors.RIGHT: lambda i, w: i + 1,
                Cells.Neighbors.DOWN: lambda i, w: i + w,
                Cells.Neighbors.LEFT: lambda i, w: i - 1,
                Cells.Neighbors.UP: lambda i, w: i - w,
                Cells.Neighbors.DOWN_RIGHT: lambda i, w: i + w + 1,
                Cells.Neighbors.DOWN_LEFT: lambda i, w: i + w - 1,
                Cells.Neighbors.UP_RIGHT: lambda i, w: i - w + 1,
                Cells.Neighbors.UP_LEFT: lambda i, w: i - w - 1}
            
            return MATHS[self](index, width)
        
        @cache
        def wrap(self, index: int, shape: tuple[int, int]) -> int:
            """Compute the index directly opposite this edge-bound `index`,
            on a 2D grid of shape `shape`, if you were to wrap "around"."""
            MATHS = {Cells.Neighbors.UP:lambda i, w, h: i + w * (h - 1),
                    Cells.Neighbors.RIGHT:lambda i, w, h: i + 1 - w,
                    Cells.Neighbors.DOWN:lambda i, w, h: i - w * (h - 1),
                    Cells.Neighbors.LEFT:lambda i, w, h: i - 1 + w}
            
            return MATHS[self](index, shape[0], shape[1])
            
    
    def __init_subclass__(cls, field: memoryview, field_shape: tuple[int, int], byte_strconv: callable = lambda x:f"{int(x):0>2x} "):
        cls._field = field
        cls._field_shape = field_shape
        cls.as_string = byte_strconv

    def __init__(self, index: int):
        self._index = index
        self.__class__.all_cells[index] = self
    
    def __str__(self):
        return self.as_string()

    def __repr__(self):
        return f"{self.__qualname__}({self._index})"
        # return f'Cell({self._index}, field={repr(self._field)}))'

    def __bytes__(self):
        return int(self).to_bytes()
    
    def __int__(self):
        return int(self._value)
    
    def __lshift__(self, other):
        return self._value << other

    def __rshift__(self, other):
        return self._value >> other

    def __and__(self, other):
        return self._value & other

    def __xor__(self, other):
        return self._value ^ other
    
    def __or__(self, other):
        return self._value | other

    def __rlshift__(self, other):
        return other << self._value

    def __rrshift__(self, other):
        return other >> self._value

    def __rand__(self, other):
        return other & self._value

    def __rxor__(self, other):
        return other ^ self._value
    
    def __ror__(self, other):
        return other | self._value
    
    def __ilshift__(self, other):
        self._field[self._index] <<= other
        return self
    
    def __irshift__(self, other):
        self._field[self._index] >>= other
        return self

    def __iand__(self, other):
        self._field[self._index] &= other
        return self

    def __ixor__(self, other):
        self._field[self._index] ^= other
        return self

    def __ior__(self, other):
        self._field[self._index] |= other
        return self
    
    def bit_is_set(self, bit: int):
        return self & int(bit)

    def set_bit(self, bit: int | IntFlag):
        # self._field[self._index] = self._value | bit
        self |= int(bit)

    def unset_bit(self, bit: int | IntFlag):
        # Have to make bit into an int instead of an IntFlag,
        # or it clobbers the bits left of the highest bit being unset
        # self._field[self._index] = self._value & ~int(bit)
        self &= ~int(bit)

    def toggle_bit(self, bit: IntFlag):
        # self._field[self._index] = self._value ^ bit
        self ^= int(bit)


class Field(bytearray):
    # CONSTANTS
    # Cell flags, set on the lower half of the byte at each index
    class Flags(IntFlag):
        # These probably shouldn't be auto, all things considered
        # IDK how auto works, and they have to be the lower 4 bits
        MINE = 0x01
        FLAG = 0x02
        REVEALED = 0x04
        CURSOR = 0x08

    # Used as return values from the Field.reveal() function
    # Makes it easier to interact with the class from the outside
    class Revealing(IntFlag):
        NOTHING_REVEALED = auto()
        ADJACENTS_REVEALED = auto()
        MINE_REVEALED = auto()

    # Making Field.Cell available to vscode
    class Cell(Cells, field=bytes(0), field_shape=(0, 0)): ...

    # COUNT_OFFSET is the number of bits the counter is shifted left in the byte
    # COUNT_MASK masks the "adjacent bombs" counter part of the byte
    # BOUNDARY_MASK masks that and the "REVEALED" bit (and "FLAG")
    #   - hardcoded because we don't use auto in Field.Flags anyway
    COUNT_OFFSET = 4
    COUNT_MASK = 0xF0
    BOUNDARY_MASK = 0xF7

    # dunder methods
    def __init__(self, source: bytes = bytes(625), shape: tuple = (25, 25), mines: int = 10, cursor: tuple = (0, 0), *args, **kwargs):
        super().__init__(source, *args, **kwargs)
        self._memoryview = memoryview(self)

        self.shape = tuple(shape)

        # class _Cell(Cells, field=self._memoryview, field_shape=self.shape): ...
        class _Cell(Cells, field=self._memoryview, field_shape=self.shape, byte_strconv=self._cell_str_format): ...
        self.Cell = _Cell

        if self.shape[0] * self.shape[1] != len(self):
            raise ValueError(f"Shape {shape} can't apply to a source of length {len(source)}")

        for i in range(len(self)):
            self.Cell(i)

        self.add_random_mines(mines, size=self.shape)
        self.cursor = self.index_at(cursor, size=self.shape)
        self.set_cursor(self.cursor)

    def __str__(self):
        pretty_indexes = [str(self.Cell.all_cells[cell]) for cell in range(len(self))]
        return "\n".join(["".join([j for j in pretty_indexes[self.shape[0] * i:self.shape[0] * i + (self.shape[0])]]) for i in range(self.shape[1])])

    def __getitem__(self, key) -> Cells:
        try:
            ret = self.Cell.all_cells[key]
        except (TypeError, IndexError, KeyError) as err:
            raise err
        else:
            return ret
    
    def __setitem__(self, key, value):
        self._memoryview[key] = value

    # Utilities
    @staticmethod
    @cache
    def index_at(coord: tuple, *, size: tuple):
        """Turn 0-indexed 2D coordinates into a 1D index"""
        if coord[0] >= size[0] or coord[0] < 0:
            raise ValueError(f"x = {size[0]} out of bounds; field size is {size}")
        if coord[1] >= size[1] or coord[1] < 0:
            raise ValueError(f"y = {[size[1]]} is out of bounds; field size is {size}")
        return coord[0] + (coord[1] * size[0])

    @staticmethod
    @cache
    def coords_of(index: int, *, size: tuple):
        """Turn 1D index into 2D coordinates"""
        return (index % size[0], index // size[0])

    # Game information functions
    def is_mine(self, index: int) -> bool:
        return bool(self[index].bit_is_set(self.Flags.MINE))

    def is_flag(self, index: int) -> bool:
        return bool(self[index].bit_is_set(self.Flags.FLAG))

    def is_revealed(self, index: int) -> bool:
        return bool(self[index].bit_is_set(self.Flags.REVEALED))
    
    def is_cursor(self, index: int) -> bool :
        return bool(self[index].bit_is_set(self.Flags.CURSOR))

    def count_adjacents(self, index: int):
        return (self[index] & Field.COUNT_MASK) >> Field.COUNT_OFFSET

    def count_flags(self):
        return len([index for index in range(len(self)) if self.is_flag(index)])

    def count_mines(self):
        return math.prod(self.shape) - len(self.get_unmined_indexes())
  
    def get_unmined_indexes(self):
        """Returns indexes to all cells without a mine."""
        return [i for i in range(len(self)) if not self.is_mine(i)]
    
    def is_boundary(self, index:int):
        return self[index] & Field.BOUNDARY_MASK

    # Game manipulation functions
    #### Pure byte manipulation
    def set_flag(self, index: int):
        self[index].set_bit(Field.Flags.FLAG)
    
    def set_revealed(self, index: int):
        self[index].set_bit(Field.Flags.REVEALED)

    def unset_flag(self, index: int):
        self[index].unset_bit(Field.Flags.FLAG)

    def unset_revealed(self, index: int):
        self[index].unset_bit(Field.Flags.REVEALED)

    def unset_cursor(self, index: int):
        self[index].unset_bit(Field.Flags.CURSOR)

    def toggle_flag(self, index: int):
        self[index].toggle_bit(Field.Flags.FLAG)

    #### Side effects
    def set_adjacents(self, index: int, value: int):
        if value > 8 or value < 0:
            raise ValueError(f"This is 2D Euclidean Minesweeper -- there physically can't be {value} mines nearby. (required: 0 >= value <= 8)")
        # Clear adjacents, then set it again with the new value
        self[index].unset_bit(Field.COUNT_MASK)
        self[index].set_bit(value << Field.COUNT_OFFSET)

    def set_mine(self, index: int):
        """Also updates neighbors' adjacent counts (mask 1111_0000)"""
        self[index].set_bit(Field.Flags.MINE)
        for neighbor in self[index].neighbors:
            self.set_adjacents(neighbor, self.count_adjacents(neighbor) + 1)

    def set_cursor(self, index: int):
        """Updates two cells (unsetting and setting cursor bit), also updates this Field's `cursor` property"""
        self[self.cursor].unset_bit(Field.Flags.CURSOR)
        self.cursor = index
        self[self.cursor].set_bit(Field.Flags.CURSOR)

    def unset_mine(self, index: int):
        """Also updates neighbors' adjacent counts (mask 1111_0000)"""
        self[index].unset_bit(Field.Flags.MINE)
        for neighbor in self[index].neighbors:
            self.set_adjacents(neighbor, self.count_adjacents(neighbor) - 1)

    # Play-time Field manipulation functions
    def move_cursor(self, direction:int | IntFlag):
        if direction & self[self.cursor].edges == 0:
            new_cursor_index = Cells.Neighbors(direction).relative_index(self.cursor, self.shape[0])
        else:
            new_cursor_index = Cells.Neighbors(direction).wrap(self.cursor, self.shape)
        self.set_cursor(new_cursor_index)
        return self.cursor

    def cursor_click(self) -> int:
        self.unset_flag(self.cursor)
        if self.is_revealed(self.cursor):
            index_flag = self.reveal_neighbors(self.cursor)
        else:
            index_flag = self.reveal(self.cursor)
        return index_flag

    def cursor_rclick(self):
        if self.is_revealed(self.cursor):
            print("Marking an empty square is a waste of flags.")
            return -1
        else:
            self.toggle_flag(self.cursor)
            return self.cursor
    
    def add_random_mines(self, number: int = 10, *, size: tuple):
        """Adds mines to random cells """
        for i in random.sample(self.get_unmined_indexes(), k=number):
            self.set_mine(i)

# Algorithm from https://en.wikipedia.org/w/index.php?title=Flood_fill&oldid=1172819476#Span_filling
# I kind of bodged in revealing the numbers at the edges...
    def reveal(self, index: int, anim: bool=False):
        def _reveal(coords):
            id = self.index_at(coords, size=self.shape)
            self.set_revealed(id)
            if anim:
                self.display()
            return id
        
        def _inside(coords):
            return (coords[0] >= 0 and coords[0] < self.shape[0]
                and coords[1] >= 0 and coords[1] < self.shape[1]
                and not self.is_boundary(self.index_at((coords[0], coords[1]), size=self.shape)))

        initial = self.coords_of(index, size=self.shape)

        if not _inside(initial):
            self.set_revealed(index)
            if self.is_mine(index):
                return self.Revealing.MINE_REVEALED
            else:
                return self.Revealing.ADJACENTS_REVEALED

        stack = []
        stack.append((initial[0], initial[0], initial[1], 1))
        stack.append((initial[0], initial[0], max(0, initial[1] - 1), -1))
        revealed_cells = []
        while stack != []:
            item = stack.pop(0)
            x = item[0]
            x1 = item[0]
            x2 = item[1]
            y = item[2]
            dy = item[3]
            if _inside((x, y)):
                while _inside((x - 1, y)):
                    revealed_cells.append(_reveal((x - 1, y)))
                    x = x - 1
                if x < x1:
                    stack.append((x, x1 - 1, y - dy, -dy))
            while x1 <= x2:
                while _inside((x1, y)):
                    revealed_cells.append(_reveal((x1, y)))
                    x1 = x1 + 1
                if x1 > x:
                    stack.append((x, x1 - 1, y + dy, dy))
                if x1 - 1 > x2:
                    stack.append((x2 + 1, x1 - 1, y - dy, -dy))
                x1 = x1 + 1
                while x1 < x2 and not _inside((x1, y)):
                    x1 = x1 + 1
                x = x1
        
        for cell in revealed_cells:
            self.reveal_neighbors(cell)
            if anim:
                self.display()
            

        return self.Revealing.ADJACENTS_REVEALED | self.Revealing.NOTHING_REVEALED

    def reveal_neighbors(self, index, *args, **kwargs):
        revealed = 0
        for neighbor in self[index].neighbors:
            if self.is_flag(neighbor) or self.is_revealed(neighbor):
                revealed |= self.Revealing.NOTHING_REVEALED
            elif self.is_mine(neighbor):
                self.set_revealed(neighbor)
                revealed |= self.Revealing.MINE_REVEALED
            elif self.count_adjacents(neighbor) > 0:
                self.set_revealed(neighbor)
                revealed |= self.Revealing.ADJACENTS_REVEALED
            else:
                self.reveal(neighbor)
                revealed |= self.Revealing.NOTHING_REVEALED
        return revealed

    def reveal_all(self):
        for i in range(len(self)):
            self.set_revealed(i)
    
    def display(self):
        header = f"MINES: {self.count_mines():>3} | FLAGS: {self.count_flags()}"
        pad = "".join(repeat("-", ((self.shape[0] * 3) - len(str(header)) - 4) // 2))

        os.system('cls' if os.name == 'nt' else 'clear')
        print(f"\n{pad}  {header}  {pad}")
        print(self)
        
    @staticmethod
    def _cell_str_format(byte: int):
        def char_map(byte: int):
            if (byte) & (Field.Flags.REVEALED | Field.Flags.FLAG) == Field.Flags(0):
                return "#"
            elif (byte) & (Field.Flags.REVEALED | Field.Flags.FLAG) == (Field.Flags.FLAG):
                return "!"
            elif (byte) & (Field.Flags.REVEALED | Field.Flags.MINE) == (Field.Flags.REVEALED | Field.Flags.MINE):
                return "X"
            elif (byte) & (Field.Flags.REVEALED | Field.Flags.MINE) == Field.Flags.REVEALED:
                return str((byte & Field.COUNT_MASK) >> Field.COUNT_OFFSET).replace("0", " ")

        if byte & Field.Flags.CURSOR == Field.Flags.CURSOR:
            return f"{char_map(byte):#^3}"
        elif byte & Field.Flags.FLAG or (byte & Field.Flags.MINE and byte & Field.Flags.REVEALED):
            return f"{char_map(byte):_^3}"
        else:
            return f"{char_map(byte): ^3}"

def run_game(shape, mines):
    game = Field(source=bytes(math.prod(shape)), shape=shape, mines=mines, cursor=(0, 0))
    # game = Field()
    game.display()
    GAME_OVER = False
    while not GAME_OVER:
        keypress = int(msvcrt.getch().hex(), base=16)
        try:
            if keypress == SpecialKeys.SPECIAL1 or keypress == SpecialKeys.SPECIAL2:
                keypress = int(msvcrt.getch().hex(), base=16)
                real_keypress = SpecialKeys(keypress)
            else:
                real_keypress = Keys(keypress)
        except ValueError:
            continue

        match real_keypress:
            case SpecialKeys.UP | Keys.EIGHT:
                game.move_cursor(Cells.Neighbors.UP)
            case SpecialKeys.RIGHT | Keys.SIX:
                game.move_cursor(Cells.Neighbors.RIGHT)
            case SpecialKeys.DOWN | Keys.TWO:
                game.move_cursor(Cells.Neighbors.DOWN)
            case SpecialKeys.LEFT | Keys.FOUR:
                game.move_cursor(Cells.Neighbors.LEFT)
            case Keys.ESCAPE:
                return -1
            case Keys.ENTER:
                # revealed = game.reveal(game.cursor, size=game.size, first=True)
                revealed = game.cursor_click()
                if revealed & Field.Revealing.MINE_REVEALED:
                    game.reveal_all()
                    game.display()
                    GAME_OVER = True
                    print("\n\nG A M E #X# O V E R\n\n")
                    return 0
                else:
                    pass
            case Keys.SPACE:
                # game.toggle_flag(game.cursor)
                game.cursor_rclick()
                if all(game[cell] & (game.BOUNDARY_MASK & ~game.COUNT_MASK) == game.Flags.MINE | game.Flags.FLAG
                    for cell in set(range(len(game))).difference(set(game.get_unmined_indexes()))):
                    game.reveal_all()
                    game.display()
                    GAME_OVER = True
                    print("\n\nY O U #!# W I N\n\n")
                    return 1

        game.display()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="Bytesweeper",
        description="Lightweight Python 3.10 Minesweeper, using one byte per cell.")
    parser.add_argument("-s", "--size",
        action="store",
        nargs=2,
        default=[10, 10],
        help="X Y size of mine field, not more than 494 cells",
        metavar="N",
        dest="size",
        type=int)
    parser.add_argument("-m", "--mines",
        action="store",
        default=10,
        help="number of mines",
        metavar="N",
        dest="mines",
        type=int)

    argv = parser.parse_args()
    print(argv)

    run_game(argv.size, argv.mines)