"""A selection of small games that can be played from the interpreter

```python
>>> import SKgames
>>> SKgames.run_menu()
```
"""
from functools import partial
from menu import AMenu
import asyncio

from .mines import mines
from .infinite_wordle import infwordle

def run_mines(height=10, width=10, mine_count=10):
    """Minesweeper"""
    return mines.run_game((width, height), mine_count)

def run_wordle():
    """Wordle"""
    return infwordle.run_game()

MENU = AMenu(mines = run_mines, wordle = run_wordle)

def run_menu():
    asyncio.run(MENU.loop())