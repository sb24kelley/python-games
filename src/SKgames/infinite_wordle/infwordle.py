#!/bin/python3
from copy import deepcopy
import random
from string import ascii_uppercase

class Puzzle:
    """A little wordle game.

    Play in interpreter:

        >>> import infwordle
        >>> game = infwordle.Puzzle()
        >>> game.input_guess('wheat')
        False
        >>> print(game)
        🌕🌕🌓🌓🌕  wheat

        Untried letters:
        B  C  D  F  G  I  J  K  L  M  N  O  P  Q  R  S  U  V  X  Y  Z  
        Known letters:
        A  E  
        Known bad letters:
        H  T  W  


        >>> 
    
    Run the script to play in console: ./infwordle.py
    """
    def __init__(self,
                word_length = 5,
                max_guesses = 6,
                *,
                word = '',
                dictionary = None
            ):
        """Just call Puzzle() for the standard.

        Args:
            word_length (int, optional): The length of the answer.
                            Defaults to 5.
            max_guesses (int, optional): Maximum number of guesses.
                            Defaults to 6.
            word (str, optional): The answer; for testing.
                            Defaults to a random word from dictionary.
            dictionary (Dictionary, optional): The dictionary to use.
                            Lets Dictionary() decide by default.

        Raises:
            Dictionary.NotInDictionaryError: `word` is provided, but
                            not in the `dictionary`
        """
        self.guesses = []
        self.word_length = int(word_length)
        self.max_guesses = int(max_guesses)
        self.remaining_chars = set(ascii_uppercase)
        self.guessed_chars = set()
        self.missed_chars = set()

        if dictionary == None:
            self.dictionary = Dictionary()
        else:
            self.dictionary = Dictionary(file=dictionary)
        
        if word == '':
            self.word = self.dictionary.rand_word(word_length).lower()
        elif word.lower() not in self.dictionary.wordlist:
            raise Dictionary.NotInDictionaryError(word)
        else:
            self.word = word.lower()

    def __str__(self):
        """Provide a nice friendly output on str() conversion"""
        chr_fun = lambda c: '{:3}'.format(c)
        gameboard_fun = lambda guess: '{:7}{}'.format(guess[1], guess[0])

        chars_known_set = sorted(
                            self.guessed_chars.difference(self.missed_chars)
                        )

        chars_remaining = [chr_fun(c) for c in sorted(self.remaining_chars)]
        chars_known = [chr_fun(c)for c in chars_known_set]
        chars_missed = [chr_fun(c) for c in sorted(self.missed_chars)]
        gameboard = [gameboard_fun(guess) for guess in self.guesses]

        chars_remaining_str = 'Untried letters:\n' + ''.join(chars_remaining)
        chars_known_str = 'Known letters:\n' + ''.join(chars_known)
        chars_missed_str = 'Known bad letters:\n' + ''.join(chars_missed)
        gameboard_str = '\n'.join(gameboard)

        ret_str = [gameboard_str,
                    '',
                    chars_remaining_str,
                    chars_known_str,
                    chars_missed_str,
                    '\n'
                ]
        return '\n'.join(ret_str)

    def __repr__(self):
        ret_str = [self.word,
                    str(self.guesses),
                    str(self.remaining_chars),
                    str(self.guessed_chars),
                    str(self.missed_chars)
                ]
        return '\n'.join(ret_str)
    
    def input_guess(self, guess: str):
        """Calls try_word on a guess, and modifies the Puzzle
        
        Raises:
            GameOverError if the game is over,
            GuessLengthError if the guess doesn't match
                this puzzle's word_length.
            Dictionary.NotInDictionaryError if the guess isn't in
                this Puzzle's dictionary
        """
        # guess-related errors raised in try_word
        results = Puzzle.try_word(guess, **self._get_state())
        
        self.guesses.append((results[0], results[1]))
        self.missed_chars = self.missed_chars.union(results[2])
        self.guessed_chars = self.guessed_chars.union(results[3])
        self.remaining_chars = self.remaining_chars.difference(self.guessed_chars)
        if guess != self.word:
            if len(self.guesses) == self.max_guesses:
                raise self.GameOverError
            else:
                return False
        else:
            return True
    
    def _get_state(self):
        ret = {
            'missed_chars':self.missed_chars,
            'guessed_chars':self.guessed_chars,
            'word_length':self.word_length,
            'dictionary':self.dictionary,
            'word':self.word
        }
        return ret

    @staticmethod
    def try_word(guess: str,
                dictionary=None,
                word: str=None,
                word_length: int=None,
                missed_chars: set=None,
                guessed_chars: set=None,
                *,
                puzzle=None
            ):
        """Takes the state of a puzzle, returns a tuple of result.

        Takes care to be functional for no particular reason. Makes a
        deepcopy() of any input sets; doesn't modify the originals.

        If the `puzzle` keyword is passed a Puzzle object, try_word
        will operate on the Puzzle's _get_state() and ignore the
        other optional parameters. Other objects in `puzzle` are
        ignored.

        Will throw some standard Python exception if you don't use it
        right.

        Args:
            dictionary (Dictionary): The Dictionary of words.
            guess (str): The 'guess' word.
            word (str, optional): The answer.
            word_length (int, optional): The answer length.
            missed_chars (set, optional): a char set to add to.
                            Defaults to an empty set.
            guessed_chars (set, optional): a char set to add to.
                            Defaults to an empty set.
            puzzle (Puzzle, optional): a Puzzle to use for state.
                            Defaults to None.

        Raises:
            Puzzle.GuessLengthError: The `guess` length didn't
                            match `word_length`
            Dictionary.NotInDictionaryError: The `guess` wasn't
                            in the `dictionary`

        Returns:
            tuple: (
                        guess:str,
                        guess_string:str,
                        missed_chars:set,
                        guessed_chars:set
                    )
            `guess` is the input `guess`
            `guess_string` represents the correctness of each letter
            `missed_chars` and `guessed_chars` are the new sets.
        """
        if isinstance(puzzle, Puzzle):
            return Puzzle.try_word(guess, **puzzle._get_state())
        # set constants
        blank_character = '\U0001F315'
        correct_character = '\U0001F311'
        almost_character = '\U0001F313'
        # set defaults and process input
        if missed_chars == None:
            missed_chars = set()
        else:
            missed_chars = deepcopy(missed_chars)
        if guessed_chars == None:
            guessed_chars = set()
        else:
            guessed_chars = deepcopy(guessed_chars)
        guess_string = blank_character * word_length
        guess_string = list(guess_string)
        guess = guess.lower()
        # guess logic
        if len(guess) != word_length:
            raise Puzzle.GuessLengthError(guess, len(guess), word_length)
        elif guess not in dictionary.wordlist:
            raise Dictionary.NotInDictionaryError(guess)
        for i, c in enumerate(guess):
            if c in word:
                if c == word[i]:
                    guess_string[i] = correct_character
                else:
                    guess_string[i] = almost_character
            else:
                missed_chars.add(c.upper())
            guessed_chars.add(c.upper())
        guess_string = ''.join(guess_string)
        return (guess, guess_string, missed_chars, guessed_chars)

    class GuessLengthError(ValueError):
        """The guess is the wrong length"""
        pass

    class GameOverError(Exception):
        """The game is over"""
        pass

class Dictionary:
    """Represents a dictionary on disk.

    A dictionary is a plain-text file with one word per line.
    """
    def __init__(self, file = "web2.txt"):
        _dir = "\\".join(__file__.split("\\")[:-1] + ['dict\\'])
        with open(_dir + file, 'r') as dictionary:
            self.wordlist = dictionary.read().splitlines()
    
    def rand_word(self, length = 5):
        """Return a random word from this dictionary of length `length`"""
        return random.choice([w for w in self.wordlist if len(w) == length])

    class NotInDictionaryError(ValueError): ...

def run_game():
    game_done = False
    puzzle = Puzzle()
    while game_done == False:
        print(puzzle)
        guess = input("Type your guess: ")
        try:
            guess_correct = puzzle.input_guess(guess)
        except Puzzle.GuessLengthError:
            print("Guess was wrong length.\n")
        except Dictionary.NotInDictionaryError:
            print("That word isn't in the dictionary.\n")
        except Puzzle.GameOverError:
            print(puzzle)
            print("GAME OVER. Try again. The word was {}".format(puzzle.word.upper()))
            game_done = True
            return 0
        except Exception as e:
            raise e
        else:
            if guess_correct:
                print(puzzle)
                print("YOU WIN!")
                game_done = True
                return 1
        finally:
            continue

if __name__ == '__main__':
    run_game()