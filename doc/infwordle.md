# Description
This is the same game as Wordle, but you can play more than once a day. It uses SOWPODS dictionary, which is a tournament Scrabble dictionary used in Europe. So there are some odd words in there. If you want, you can supply your own dictionary:
1. Create a text file with one word per line, and put it in /dict.
1. Edit line 7 of `infwordle.py`:
    1. `    def __init__(self, file = '[YOUR_DICTIONARY_FILE]'):`

My dictionary sources:
1. sowpods.txt: https://www.wordgamedictionary.com/sowpods/download/sowpods.txt
1. web2.txt: `cat /usr/share/dict/web2 > web2.txt`

# Installation

1. Install Python 3
1. Clone or download this repository
1. Open your terminal or command prompt and run `infwordle.py`
1. Profit

# Output example:
The program only runs in the console right now. I'm planning to add some kind of graphical interface, but the console functionality will still be there.
Here's an example of output on the final round of the game, including my guess (levee):

    Type your guess: levee
    🌕🌕🌕🌑🌕  pared
    🌕🌕🌕🌑🌕  mines
    🌕🌕🌓🌑🌕  holey
    🌕🌕🌕🌑🌓  kugel
    🌑🌑🌑🌑🌓  level
    🌑🌑🌑🌑🌑  levee

    Untried letters:
    B  C  F  J  Q  T  W  X  Z  
    Known letters:
    E  L  V  
    Known bad letters:
    A  D  G  H  I  K  M  N  O  P  R  S  U  Y  


    YOU WIN!
